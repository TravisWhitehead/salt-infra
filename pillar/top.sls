base:
  '*':
    - salt.minion

  'salt*':
    - salt
    - ssh_keys.travis

  'workstation*':
    - pkgs.gdb-peda
    - pkgs.weechat
