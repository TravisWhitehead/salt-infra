salt:
  master:
    fileserver_backend:
      - roots
      - git
    file_roots:
      base:
        - /srv/salt/states
    pillar_roots:
      base:
        - /srv/salt/pillar
    gitfs_remotes:
      - https://github.com/TravisWhitehead/python2-formula.git
      - https://github.com/TravisWhitehead/salt-formula-linux.git
      - https://github.com/TravisWhitehead/salt-transmission-formula.git
      - https://github.com/TravisWhitehead/salt-formula.git
      - https://github.com/TravisWhitehead/vim-formula.git
