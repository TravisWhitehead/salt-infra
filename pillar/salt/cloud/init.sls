include:
  - salt.cloud.profiles
  - salt.cloud.providers

# update_cachedir seems needed for salt-ssh to use the cloud roster
salt:
  cloud:
    update_cachedir: True
