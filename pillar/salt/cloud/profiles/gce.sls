salt:
  cloud:
    profiles:
      gce.conf:
        debian_small:
          image: debian-9
          location: us-west1-b
          provider: gce
          size: g1-small
          use_persistent_disk: True
