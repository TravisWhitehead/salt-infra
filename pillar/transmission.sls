# https://github.com/transmission/transmission/wiki/Editing-Configuration-Files 

transmission_daemon_settings:
  cache-size-mb: 16
  download-dir: /home/plex
  incomplete-dir: /home/transmission
  incomplete-dir-enabled: true
  rpc-authentication-required: true
  rpc-port: 9091
  peer-port: 51413
  port-forwarding-enabled: true
  rename-partial-files: true
