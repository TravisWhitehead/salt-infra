# Put packages that don't need any other states here, otherwise use pkgs/
workstation_packages:
  pkg.installed:
    - pkgs:
      - arc-theme
      - clang
      - clang-tools-extra
      - emacs
      - gcc
      - mosh
      - premake
      - screen
      - syncthing-gtk
      - vagrant
      - vim-enhanced
      - weechat
