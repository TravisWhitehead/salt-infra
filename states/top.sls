base:
  '*':
    - base
    - salt.minion

  'salt*':
    - salt.cloud
    - _salt.master

  'workstation*':
    - pkgs.cookiecutter
    - pkgs.docker
    - pkgs.gdb-peda
    - pkgs.syncthing
    - pkgs.terminator
    - pkgs.weechat
    - vim.salt
    - workstation
