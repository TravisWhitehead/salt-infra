# TODO: This fails when it already exists
create_salt_firewall_rule:
  salt.runner:
    - name: cloud.action
    - func: create_fwrule
    - provider: gce
    - kwarg:
        name: allow-salt
        allow: 'tcp:4505,tcp:4506'
        dst_tags: 'salt'

provision_cloud_map:
  salt.runner:
    - name: cloud.map_run
    - path: /srv/salt/pillar/salt/cloud/map.sls
    - kwargs:
        parallel: True

highstate:
  salt.state:
    - tgt: '*'
    - highstate: True
