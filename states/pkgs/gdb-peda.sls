gdb:
  pkg.installed

peda_repo:
  git.latest:
    - name: https://github.com/longld/peda.git
    - target: /opt/peda

{% set gdb_peda = salt['pillar.get']('gdb_peda') %}

{% for user in gdb_peda.users %}
"/home/{{ user }}/.gdbinit":
  file.managed:
    - contents:
      - source /opt/peda/peda.py
    - mode: 700
    - user: {{ user }}
    - group: {{ user }}
    - create: True
{% endfor %}
