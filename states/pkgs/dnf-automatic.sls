dnf-automatic:
  pkg.installed

# Configure dnf-automatic to apply security updates automatically
/etc/dnf/automatic.conf:
  ini.options_present:
    - sections:
        commands:
          upgrade_type: 'security'
          apply_updates: 'yes'

dnf-automatic.timer:
  service.running:
    - enable: True
