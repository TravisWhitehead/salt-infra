{% set syncthing = salt['grains.filter_by']({
  'default': 'syncthing',
  'RedHat': 'syncthing-gtk'
}) %}

{{ syncthing }}:
  pkg.installed
