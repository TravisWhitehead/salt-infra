weechat:
  pkg.installed

{% set weechat_notify_send = pillar['weechat']['notify_send'] %}

weechat_notify_send_dependencies:
  pkg.installed:
    - pkgs:
      - libnotify

{{ weechat_notify_send.directory }}:
  file.directory

{{ weechat_notify_send.directory }}/notify_send.py:
  file.managed:
    - source: "https://raw.githubusercontent.com/s3rvac/weechat-notify-send/v{{ weechat_notify_send.version }}/notify_send.py"
    - source_hash: 8c268bbe7b1dc820b4c0dd6f8e5e62c8e136207000f21b1f8be8fb98c2daedac

{% for user in weechat_notify_send.users %}
/home/{{ user }}/.weechat/python/autoload:
  file.directory:
    - makedirs: True

"/home/{{ user }}/.weechat/python/autoload/notify_send.py":
  file.symlink:
    - target: {{ weechat_notify_send.directory }}/notify_send.py
    - user: {{ user }}
{% endfor %}
