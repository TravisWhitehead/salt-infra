cockpit_packages:
  pkg.installed:
    - pkgs:
      - cockpit
      - cockpit-pcp

cockpit.socket:
  service.running:
    - enable: True

allow_cockpit:
  firewalld.present:
    - ports:
      - 9090/tcp
