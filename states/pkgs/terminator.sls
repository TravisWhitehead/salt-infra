terminator:
  pkg.installed

/etc/skel/.config/terminator/config:
  file.managed:
    - source: salt://files/terminator.config
    - user: root
    - group: root
    - makedirs: True
