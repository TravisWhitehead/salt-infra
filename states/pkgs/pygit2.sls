# RedHat family uses its packages over pip

{% if grains['os_family'] == 'RedHat' %}
python-pygit2:
  pkg.installed
{% endif %}


# Debian uses pygit2 from pip because 9's packaged version is affected by this bug:
# https://gist.github.com/aleixripoll/089259239bb8b1641cffe6b8ba858291

{% if grains['os_family'] == 'Debian' %}
include:
  - python.pip
  - repo.stretch-backports

{% set libgit2_pkg = salt['grains.filter_by']({
    'Debian': 'libgit2-dev',
    'RedHat': 'libgit2-devel'
}, grain='os_family') %}

{{ libgit2_pkg }}:
  pkg.installed:
    - fromrepo: stretch-backports

# XXX: Pin pygit2 to work around https://github.com/saltstack/salt/issues/51270
# Unfortunately this version of pygit2 requires a newer libgit2-dev version from backports
pygit2_pin_workaround:
  pip.installed:
    - name: pygit2 < 0.27.4
    - require:
      - pkg: python2-pip
      - pkg: {{ libgit2_pkg }}
{% endif %}
