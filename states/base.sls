# Contains states common to all salt-managed hosts

America/Los_Angeles:
  timezone.system

{% if grains['os_family'] == 'RedHat' %}
include:
  - pkgs.dnf-automatic
{% endif %}
