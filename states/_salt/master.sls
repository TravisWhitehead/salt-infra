include:
  - pkgs.pygit2
  - salt.master

install_salt_states_and_pillars:
  git.latest:
    - name: https://gitlab.com/TravisWhitehead/salt-infra.git
    - target: /srv/salt
    - user: root

git_group:
  group.present:
    - name: git
    - system: True

git_user:
  user.present:
    - name: git
    - gid_from_name: True
    - system: True

/srv/salt:
  file.directory:
    - user: root
    - group: git
    - dir_mode: 775
    - file_mode: 664
    - recurse:
      - user
      - group
      - mode

{{ pillar['ssh_keys']['travis'] }}:
  ssh_auth.present:
    - user: git

/etc/salt/gpgkeys:
  file.directory:
    - user: root
    - group: root
    - mode: 700
