# Salt Infra
Salt configurations for my personal infra. 
This repo will to grow & improve as I continue to learn salt.

If you see anything that can be improved, please open an issue.

### Notes on Repo Layout
* This repository contains both the state tree (in `states/`) and pillar data (in `pillar/`).
* Some folders in the `states/` tree are prefixed with an underscore (e.g. `_salt`); these serve as local wrappers to upstream formulas. The underscores prevent namespace collisions.

### Bootstrapping Salt Master on GCE using Salt Cloud

[Salt Cloud](https://docs.saltstack.com/en/latest/topics/cloud/) can be used to provision VMs
defined in the map file `pillar/salt/cloud/map.sls`. The current configuration only supports
Google Compute Engine (GCE). To boostrap a new Salt Master on GCE, modify the Salt
Cloud configuration for your project & service account, use the Salt formula to install Salt Cloud
and the configuration onto your system, set up an SSH key for Salt Cloud to use, and provision the
map file:

1. Follow Salt Cloud's [documentation for GCE](https://docs.saltstack.com/en/latest/topics/cloud/gce.html#getting-started-with-google-compute-engine) to set up a service account.

2. Edit `pillar/salt/cloud/providers/gce.sls` to use your [GCE project ID and service account.](https://docs.saltstack.com/en/latest/topics/cloud/gce.html#getting-started-with-google-compute-engine)

3. Generate an SSH keypair for Salt Cloud to use and [add the public key to GCE project
metadata](https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys). (Don't use
`root` for the key's username or you might not be able to connect.) Edit
`pillar/salt/cloud/providers/gce.sls` and set `ssh_keyfile` to the private key's path and
`ssh_username` to the username specified in metadata.

4. Install your fork of the salt formula (see [formula installation docs](https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#installation)):
`https://github.com/TravisWhitehead/salt-formula.git`
The [salt formula](https://github.com/TravisWhitehead/salt-formula) is used to install Salt Cloud
and configuration (like providers and profiles) from this repo onto the local system.

5. Install the Salt Cloud providers and profiles by applying the
[salt formula](https://github.com/TravisWhitehead/salt-formula) in masterless mode using this repo's
pillar data: `sudo salt-call --local --id=salt --file-root=states/ --pillar-root=pillar/ state.apply salt.cloud`

6. Provision the map file: `sudo salt-cloud -m pillar/salt/cloud/map.sls -P`

**TODO:** `_salt.master` still needs to be applied to the new master and salt minion keys need to be
accepted. It would be nice to automate the remaining setup of the master with `salt-ssh`.
